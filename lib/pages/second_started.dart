import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class SecondStarted extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        body: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.only(left: 40, top: 40),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Health First.",
                  style: GoogleFonts.poppins(
                      fontSize: 24, fontWeight: FontWeight.w700),
                ),
                SizedBox(
                  height: 16,
                ),
                Text(
                  "Exercise together with our best \ncommunity fit in the world",
                  style: GoogleFonts.poppins(
                      fontSize: 16, color: Color(0xff828284)),
                ),
                SizedBox(
                  height: 60,
                ),
                Image.asset(
                  "assets/gallery.png",
                  width: 320,
                  fit: BoxFit.cover,
                ),
                SizedBox(
                  height: 70,
                ),
                ElevatedButton(onPressed: () {}, child: Text("Shape My Body"))
              ],
            ),
          ),
        ));
  }
}
