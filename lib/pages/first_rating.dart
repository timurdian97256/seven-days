import 'package:flutter/material.dart';
import 'package:seven_days/widgets/theme.dart';

class FirstRating extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xff181925),
      body: Padding(
        padding: const EdgeInsets.only(top: 70, left: 40, right: 40),
        child: Column(
          children: [
            Center(
              child: Image.asset(
                'assets/pizza.png',
                width: 160,
                height: 160,
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Text(
              "Pizza Ballado",
              style: foodTextStyle,
            ),
            Text(
              "\$90,00",
              style: pricingTextStyle,
            ),
            SizedBox(
              height: 70,
            ),
            Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              Text(
                "Was it delicious?",
                style: rateTextStyle,
              ),
              SizedBox(
                height: 15,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Image.asset(
                    "assets/emoji1.png",
                    width: 55,
                    height: 55,
                  ),
                  SizedBox(
                    width: 20,
                  ),
                  Image.asset(
                    "assets/emoji2.png",
                    width: 55,
                    height: 55,
                  ),
                  SizedBox(
                    width: 20,
                  ),
                  Image.asset(
                    "assets/emoji3.png",
                    width: 55,
                    height: 55,
                  ),
                  SizedBox(
                    width: 20,
                  ),
                  Image.asset(
                    "assets/emoji4.png",
                    width: 55,
                    height: 55,
                  ),
                ],
              )
            ]),
            SizedBox(
              height: 90,
            ),
            GestureDetector(
              child: Container(
                width: 211,
                height: 55,
                decoration: BoxDecoration(
                    color: Color(0xff34D186),
                    borderRadius: BorderRadius.circular(60)),
                child: Align(
                  child: Text(
                    "Rate Now",
                    style: rateTextStyle,
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
